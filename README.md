# Towards Preemptive Detection of Depression and Anxiety in Twitter  #

### The DATD (Depression and Anxiety in Twitter Dataset) ###

The dataset comprises:  

1) Two training sets that are to be used independently of one another  

* DATD_training.csv  
* DATD+Rand_training.csv  

2) A common test set:  

* DATD_and_DATD+Rand_test.csv  

#### All enquiries: ####
David Owen: owendw1@cardiff.ac.uk  
Jose Camacho Collados: camachocolladosj@cardiff.ac.uk  
Luis Espinosa-Anke: espinosa-ankel@cardiff.ac.uk  
